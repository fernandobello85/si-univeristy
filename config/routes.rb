Rails.application.routes.draw do
  # mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  # devise_scope :user do
  #   get '/users/sign_out' => 'devise/sessions#destroy'
  # end
  # resources :users, only: [:show, :destroy], :constraints => { :id => /[0-9|]+/ }
  root 'pages#welcome'
  get 'home', to: 'pages#home'
  get 'about', to: 'pages#about'
  get 'contact', to: 'pages#contact'
  resources :courses
  get 'my_courses', to: 'courses#my_courses'
  get 'enrollments', to: 'courses#enrollments'
  get 'enrolled', to: 'courses#enrolled'
  post 'enroll', to: 'grades#enroll'
  get 'edit_grade', to: 'grades#edit_grade'
  post 'update_grade', to: 'grades#update_grade'

  namespace :admin do
    resources :users
  end
end
