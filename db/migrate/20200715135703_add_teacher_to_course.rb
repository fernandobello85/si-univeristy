class AddTeacherToCourse < ActiveRecord::Migration[6.0]
  def change
    add_reference :courses, :teacher, references: :users
  end
end
