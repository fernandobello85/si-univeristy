class ChangeGradeToFloat < ActiveRecord::Migration[6.0]
  def change
    change_column :grades, :grade, :float
  end
end
