class RemoveRolesFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :teacher
    remove_column :users, :student
    remove_column :users, :admin
  end
end
