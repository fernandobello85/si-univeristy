// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
global.toastr = require("toastr")

import 'materialize-css/dist/js/materialize'
import "../stylesheets/application"
// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

document.addEventListener("turbolinks:load", function() {
  $(".dropdown-trigger").dropdown();
  $('#fade-out-panel').fadeOut(4000);
  $('input#input_text, textarea#textarea2').characterCounter();
  $('.parallax').parallax();
  $('select').formSelect();
  $('.carousel').carousel({
    fullWidth: true,
    indicators: true
  });
  var autoplay = true;
  setInterval(function() { if(autoplay) $('.carousel.carousel-slider').carousel('next'); }, 4500);
  $('.carousel.carousel-slider').hover(function(){ autoplay = false; },function(){ autoplay = true; });

  let elem = document.querySelector('#mobile-demo');
  let instance = new M.Sidenav(elem, {});
});

document.addEventListener('turbolinks:before-visit', function() {
  let elem = document.querySelector('#mobile-demo');
  let instance = M.Sidenav.getInstance(elem);
  if (instance){
    instance.destroy();
  }
});