class Grade < ApplicationRecord
  belongs_to :course
  belongs_to :user
  # validates :grade, numericality: true, inclusion: {in: 0..5, message: 'Value should be between 0 and 5'}
end
