class Course < ApplicationRecord
  has_many :grades
  has_many :users, through: :grades
  validates :short_name, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 4 }
  validates :name, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 50 }
  validates :description, uniqueness: { case_sensitive: false }, length: { maximum: 300 }

  belongs_to :teacher, class_name: 'User'
end
