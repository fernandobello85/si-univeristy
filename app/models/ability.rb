# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    can_do_admin_stuff(user) if user.admin?
    can_do_student_stuff(user) if user.student?
    can_do_teacher_stuff(user) if user.teacher?

    # if user.admin?
    #   can :manage, :all
    # else
    #   can :read, :all
    # end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end

  def can_do_admin_stuff(_user)
    can :manage, :all
  end

  def can_do_student_stuff(user)
    can :enroll, Grade
    can %i[show index enrollments], Course
    can :enrolled, Course do |course|
      course.users.include?(user)
    end
  end

  def can_do_teacher_stuff(user)
    can %i[show index my_courses], Course
    can %i[edit update enrolled], Course do |course|
      course.teacher_id == user.id
    end
  end
end
