class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  before_save :downcase_email, :downcase_role, :set_username
  validates :first_name, presence: true, length: { minimum: 5, maximum: 100 }
  validates :last_name, presence: true, length: { minimum: 5, maximum: 100 }
  VALID_EMAIL_REGEX = /\A[^@\s]+@([^@\s]+\.)+[^@\W]+\z/
  validates :email, presence: true, uniqueness: { case_sensitive: false },
                      length: { maximum: 105 }, format: { with: VALID_EMAIL_REGEX }
  validates :password, presence: true, length: { minimum: 6 }, if: :new_user?
  validates :role, presence: true, inclusion: { in: %w[Student Teacher] }, if: :new_user?

  has_many :grades
  has_many :courses, through: :grades, dependent: :destroy

  has_many :teaching_courses, class_name: "Course", foreign_key: :teacher_id

  scope(:students, -> { where(role: 'student') })
  scope(:teachers, -> { where(role: 'teacher') })

  def student?
    role == 'student'
  end

  def teacher?
    role == 'teacher'
  end

  def admin?
    is_admin?
  end

  def grade_for(course)
    Grade.where(course_id: course.id, user_id: id).last.grade || '--'
  end

  private

  def new_user?
    new_record?
  end

  def set_username
    self.username = (first_name.slice(0) + last_name.split[0]).downcase
  end

  def downcase_email
    self.email = email.downcase
  end

  def downcase_role
    self.role = role.downcase
  end
end
