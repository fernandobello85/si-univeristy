class PagesController < ApplicationController
  skip_before_action :authenticate_user!, :only => [:welcome]
  before_action :find_user, only: [:home]

  def welcome
    redirect_to home_path if user_signed_in?
  end

  def home
  end

  def about
  end

  def contact
  end

  private

  def find_user
    @user = User.find(current_user.id)
  end
end
