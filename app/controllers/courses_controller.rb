class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy, :enrolled]
  before_action :set_teachers, only: [:new, :edit]
  load_and_authorize_resource

  def show
  end

  def index
    @courses = Course.all.order(created_at: :desc)
  end

  def my_courses
    @courses = Course.where(teacher_id: current_user.id).order(created_at: :desc)
  end

  def enrollments
    @enrollments = current_user.courses.order(short_name: :asc)
  end

  def enrolled
    @enrolled = @course.users
  end

  def new
    @course = Course.new
  end

  def edit
  end

  def create
    @course = Course.new(course_params)

    if @course.save
      flash[:success] = 'Course was successfully created.'
      redirect_to courses_path
    else
      flash[:alert] = 'An error occurred while course creation.'
      redirect_to new_course_path
    end
  end

  def update
    if @course.update(course_params)
      flash[:success] = 'Course was successfully updated.'
      redirect_to course_path
    else
      flash[:alert] = 'An error occurred while the update was performed.'
      redirect_to edit_course_path
    end
  end

  private

  def set_course
    @course = Course.find(params[:id])
  end

  def set_teachers
    @teachers = User.teachers
  end

  def course_params
    params.require(:course).permit(:short_name, :name, :description, :teacher_id)
  end
end
