class Admin::BaseAdminController < ApplicationController
  before_action(:authenticate_user!)
  before_action(:check_admin_authorization)

private

  def check_admin_authorization
    return if current_user.admin?

    raise ActionController::RoutingError, 'Not Found'
  end
end
