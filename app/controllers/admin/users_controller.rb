class Admin::UsersController < Admin::BaseAdminController
  before_action :set_user, only: %i[show edit update destroy]

  def index
    @users = User.all.order(created_at: :desc)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:success] = 'User was successfully updated.'
      redirect_to admin_users_path
    else
      flash[:alert] = 'An error occurred while the update was performed.'
      redirect_to edit_user_path
    end
  end

  def destroy
    @user.destroy
    session[:user_id] = nil if current_user == @user
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :username, :num, :major, :email, :role)
  end
end
