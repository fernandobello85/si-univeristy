class GradesController < ApplicationController
  before_action :set_grade, only: [:edit_grade]

  def enroll
    course_to_add = Course.find(params[:course_id])
    user_to_enroll = User.find(params[:user_id])

    if !user_to_enroll.courses.include?(course_to_add)
      Grade.create(course_id: course_to_add.id, user_id: user_to_enroll.id)
      flash[:success] = "#{user_to_enroll.first_name + ' ' + user_to_enroll.last_name } has been successfully enrolled in #{course_to_add.name}"
      redirect_to enrollments_path
    else
      flash[:alert] = "#{user_to_enroll.email} is already enrolled in #{course_to_add.name}"
      redirect_to courses_path
    end
  end

  def edit_grade
    respond_to do |format|
      format.html
      format.js { render partial: 'grades/edit_grade' }
    end
  end

  def update_grade
    @grade = Grade.find(params[:grade])
    respond_to do |format|
      if @grade.update(grade: params[:grade_avg])
        format.js { render partial: 'grades/current_grade' }
      else
        format.html { redirect_to enrolled_path(id: @grade.course), alert: @grade.errors[:grade][0] }
      end
    end
  end

  private

  def set_grade
    @grade = Grade.where(course_id: params[:course_id], user_id: params[:user_id]).last
  end
end
