# README

SI-University is a Ruby On Rails web application that works as a basic university System Information.
The main idea is to allow students to enroll different courses tough by a teacher. So one course has one teacher and many students. A teacher can tough many courses. A student can enroll many courses.

App Requirements :

- Implement roles (Admin, Teacher, Student) for the different account that can be registered in the App.
- Restrict/allow access to different ressources of the App based on the user role.
- Only Admin can create, edit or delete courses.
- Teachers can edit some course information.
- Students can enroll courses.
- A new course must have an associated teacher.
- Teachers can list their courses and the enrolled students.
- Teachers can add grade to their students.


Technical environment :
- Ruby version 2.6.3
- Rails version 6.0.3.2
- Materialize-CSS 1.0.0
- Toastr 2.1.4
- Cancancan 
- Devise
- AWS S3

[SI-University](https://si-university.herokuapp.com/)

teacher@university.net

student@university.net
